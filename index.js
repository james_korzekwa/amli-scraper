var Nightmare = require('nightmare');
var chalk = require('chalk');

var amli = new Nightmare({show: false});

amli
    .goto('https://www.amli.com/apartments/austin/central-austin/austin/mueller/floorplans')
    .wait("input[value='a5']")
    .click("input[value='a5']")
    .wait("input[value='a5']")
    .wait(function() {
    	if (document.querySelector("input[value='a5']")) {
            return document.querySelector("input[value='a5']")
					.parentNode.parentNode.parentNode.parentNode.className == 'fpItemPicked'
        }
        return false
	})
	.wait('#ContentMain_GetQuote0')
	.click('#ContentMain_GetQuote0')
	.wait('#ContentMain_pageIframe')
	.evaluate(function () {
		return document.querySelector("#ContentMain_pageIframe").src;
	})
    .end()
    .then(function (iframeUrl) {
		var quote = new Nightmare({show: false});
        quote
			.goto(iframeUrl)
			.wait()
			.evaluate(function () {
				var resp = {
                    length: document.querySelectorAll("#PricingGridView1_GridView1 tr").length,
					data: document.querySelectorAll("#PricingGridView1_GridView1 tr")
            	};

                var leaseCost = [];
                for (var i = 0; i < resp.length; i++) {
                	leaseCost.push(resp.data[i].innerText.split('\t'))
                }

                var rows = [];
                for (var i = 0; i < document.querySelectorAll("#PricingGridView1_DataList1 tr").length; i++) {
                    rows[i] = document.querySelectorAll("#PricingGridView1_DataList1 tr")[i].innerText.replace('\t', '')
                }

                return {
                	leaseCost: leaseCost,
                    headers: document.querySelectorAll("#PricingGridView1_StartDates tr")[0].innerText.split('\t'),
					rows: rows
                }
			})
			.end()
			.then(function (leaseData) {

				var least = 10000;
                for (var i = 0; i < leaseData.leaseCost.length; i++) {
                    for (var k = 0; k < leaseData.leaseCost[i].length; k++) {
                        if (leaseData.leaseCost[i][k] < least) {
                        	least = leaseData.leaseCost[i][k];
						}
                    }
                }

                var str = '\t\t';
                for (var k = 0; k < leaseData.headers.length; k++) {
                    str += leaseData.headers[k]+'\t'
                }
                console.log(chalk.red(str));

                for (var i = 0; i < leaseData.leaseCost.length; i++) {
                	var str = chalk.red(leaseData.rows[i]+'\t');
                	for (var k = 0; k < leaseData.leaseCost[i].length; k++) {
                		if (leaseData.leaseCost[i][k] <= least) {
                            str += chalk.green(leaseData.leaseCost[i][k]+'\t')
						} else {
                            str += leaseData.leaseCost[i][k]+'\t'
						}

					}
					console.log(str)
				}
			})
    });